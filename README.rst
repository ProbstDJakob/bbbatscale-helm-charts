.. role:: code-sh(code)
   :language: shell

#########################
 BBB@Scale - Helm Charts
#########################

This repo only contains the helm charts for the `BBB@Scale <https://gitlab.com/bbbatscale/bbbatscale>`_ repo.

*******
Install
*******

The chart can be installed as follows.
For more information about using `helm install`:code-sh:, see the `official documentation <https://helm.sh/docs/helm/helm_install/>`_.

.. code-block:: shell-session

   $ helm repo add bbbatscale https://gitlab.com/api/v4/projects/42010718/packages/helm/stable
   $ helm install -f override-values.yaml my-bbbatscale bbbatscale/bbbatscale
