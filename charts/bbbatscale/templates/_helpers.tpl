{{/* BBB@Scale render template values */}}
{{- define "bbbatscale.render.template" -}}
{{ if kindIs "string" .value -}}
{{ tpl .value .context }}
{{- else -}}
{{ tpl (.value | toYaml) .context }}
{{- end }}
{{- end }}

{{/* BBB@Scale returns the supplied secret or string as value for an env variable entry */}}
{{- define "bbbatscale.asEnvValueFromSecretOrString" -}}
{{ if kindIs "string" . -}}
value: {{ . | quote }}
{{- else -}}
valueFrom:
  secretKeyRef:
    name: {{ .secretName | quote }}
    key: {{ .secretKey | quote }}
{{- end }}
{{- end }}

{{/* BBB@Scale database name as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.database.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.database }}
{{- end }}

{{/* BBB@Scale database host as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.host.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.host }}
{{- end }}

{{/* BBB@Scale database password as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.password.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.password }}
{{- end }}

{{/* BBB@Scale database port as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.port.asEnvValue" -}}
{{- with .Values.postgresql.port | default 5432 }}
{{- if (kindIs "int" .) -}}
value: {{ . | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" . }}
{{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale database username as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.username.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.username }}
{{- end }}

{{/* BBB@Scale database host as value for an env variable entry */}}
{{- define "bbbatscale.redis.host.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.redis.host }}
{{- end }}

{{/* BBB@Scale database password as value for an env variable entry */}}
{{- define "bbbatscale.redis.password.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.redis.password }}
{{- end }}

{{/* BBB@Scale database port as value for an env variable entry */}}
{{- define "bbbatscale.redis.port.asEnvValue" -}}
{{- with .Values.redis.port | default 6379 }}
{{- if (kindIs "int" .) -}}
value: {{ . | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" . }}
{{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale environment variables */}}
{{- define "bbbatscale.env" -}}
- name: POSTGRES_DB
  {{- include "bbbatscale.postgresql.database.asEnvValue" . | nindent 2 }}
- name: POSTGRES_USER
  {{- include "bbbatscale.postgresql.username.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PASSWORD
  {{- include "bbbatscale.postgresql.password.asEnvValue" . | nindent 2 }}
- name: POSTGRES_HOST
  {{- include "bbbatscale.postgresql.host.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PORT
  {{- include "bbbatscale.postgresql.port.asEnvValue" . | nindent 2 }}
- name: REDIS_HOST
  {{- include "bbbatscale.redis.host.asEnvValue" . | nindent 2 }}
- name: REDIS_PORT
  {{- include "bbbatscale.redis.port.asEnvValue" . | nindent 2 }}
- name: REDIS_PASSWORD
  {{- include "bbbatscale.redis.password.asEnvValue" . | nindent 2 }}
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: djangoSecretKey
- name: RECORDINGS_SECRET
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: recordingsSecret
- name: DJANGO_SETTINGS_MODULE
  value: {{ .Values.bbbatscale.djangoSettingsModule }}
- name: SECURE_PROXY_SSL_HEADER
  value: HTTP_X_FORWARDED_PROTO
{{ if .Values.bbbatscale.webhooks.enabled -}}
- name: WEBHOOKS
  value: enabled
{{- end }}
{{ if .Values.bbbatscale.media.enabled -}}
- name: BBBATSCALE_MEDIA
  value: enabled
- name: BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS
  value: {{ .Values.bbbatscale.media.expirySeconds | quote }}
- name: BBBATSCALE_MEDIA_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: bbbatscaleMediaSecretKey
{{- end }}
- name: EMAIL_HOST
  value: {{ .Values.bbbatscale.email.host | quote }}
- name: EMAIL_PORT
  value: {{ .Values.bbbatscale.email.port | quote }}
{{- with .Values.bbbatscale.email.username }}
- name: EMAIL_HOST_USER
  value: {{ . | quote }}
{{- end }}
{{- with .Values.bbbatscale.email.password }}
- name: EMAIL_HOST_PASSWORD
  value: {{ . | quote }}
{{- end }}
- name: EMAIL_USE_TLS
  value: {{ ternary "true" "false" .Values.bbbatscale.email.useTls | quote }}
- name: EMAIL_USE_SSL
  value: {{ ternary "true" "false" .Values.bbbatscale.email.useSsl | quote }}
{{- if .Values.bbbatscale.email.sslCertPem }}
- name: EMAIL_SSL_CERTFILE
  value: /bbbatscale/BBBatScale/settings/emailSslCert.pem
{{- end }}
{{- if .Values.bbbatscale.email.sslKeyPem }}
- name: EMAIL_SSL_KEYFILE
  value: /bbbatscale/BBBatScale/settings/emailSslKey.pem
{{- end }}
- name: EMAIL_SENDER_ADDRESS
  value: {{ .Values.bbbatscale.email.senderAddress | quote }}
{{- end }}

{{/* BBB@Scale config volumes */}}
{{- define "bbbatscale.configVolumes" -}}
- name: bbbatscale-config
  configMap:
    name: bbbatscale-configmap
    defaultMode: 0444
{{- end }}

{{/* BBB@Scale config volume mounts */}}
{{- define "bbbatscale.configVolumeMounts" -}}
{{ if .Values.bbbatscale.logging.enabled -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/logging_config.py
  subPath: loggingConfig
{{ end -}}
{{ if .Values.bbbatscale.email.sslCertPem -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/emailSslCert.pem
  subPath: emailSslCertPem
{{ end -}}
{{ if .Values.bbbatscale.email.sslKeyPem -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/emailSslKey.pem
  subPath: emailSslKeyPem
{{- end }}
{{- end }}

{{/* BBB@Scale media volume */}}
{{- define "bbbatscale.mediaVolume" -}}
{{ if .Values.bbbatscale.media.enabled -}}
- name: bbbatscale-media
  persistentVolumeClaim:
    {{ if kindIs "string" .Values.bbbatscale.media.pvc -}}
    claimName: {{ include "bbbatscale.render.template" (dict "value" .Values.bbbatscale.media.pvc "context" .) | quote }}
    {{- else -}}
    claimName: bbbatscale-media-pvc
    {{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale media volume mount */}}
{{- define "bbbatscale.mediaVolumeMount" -}}
{{ if .Values.bbbatscale.media.enabled -}}
- name: bbbatscale-media
  readOnly: {{ ternary "true" "false" (default false .nginx) }}
  mountPath: {{ ternary "/var/www/bbbatscale/media/" "/bbbatscale/media/" (default false .nginx) }}
  {{ with .Values.bbbatscale.media.subPath -}}
  subPath: {{ . | quote }}
  {{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale database backup volume */}}
{{- define "bbbatscale.databaseBackupVolume" -}}
- name: bbbatscale-database-backup
  persistentVolumeClaim:
    {{ if kindIs "string" .pvc -}}
    claimName: {{ include "bbbatscale.commonLabels" (dict "value" .pvc "context" $) | quote }}
    {{- else -}}
    claimName: bbbatscale-database-backup-pvc
    {{- end }}
{{- end }}

{{/* BBB@Scale database backup mount */}}
{{- define "bbbatscale.databaseBackupVolumeMount" -}}
- name: bbbatscale-database-backup
  mountPath: /bbbatscale/database-backup/
  {{ with .subPath -}}
  subPath: {{ . | quote }}
  {{- end }}
{{- end }}

{{/* BBB@Scale common labels */}}
{{- define "bbbatscale.commonLabels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
helm.sh/chart: {{ printf "%s-%s" .Chart.Name (.Chart.Version | replace "+" "_") | quote }}
helm.sh/revision: {{ .Release.Revision | quote }}
{{- end }}
