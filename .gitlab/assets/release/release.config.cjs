"use strict";

const { constants } = require("fs");
const { access, open, readFile, writeFile } = require("fs/promises");
const { relative, resolve } = require("path");
const { BREAKING_CHANGE_TOKEN, releaseRules, sectionMap } = require("./commit-release-rules.cjs");
const Handlebars = require("handlebars");
const { generateNotes } = require("@semantic-release/release-notes-generator");
const yaml = require("js-yaml");
const { execFileSync } = require("child_process");

const repoDir = process.env["CI_PROJECT_DIR"] ?? resolve(__dirname, "..", "..", "..");
const chartPath = resolve(repoDir, "charts", "bbbatscale");
const commitAssetPaths = {
  chartYamlPath: resolve(chartPath, "Chart.yaml"),
  changelogPath: resolve(repoDir, "CHANGELOG.rst"),
};

const ownerFormat = "{{#if this.owner}}{{~this.owner}}{{else}}{{~@root.owner}}{{/if}}";
const hostFormat = "{{~@root.host}}";
const repositoryFormat = "{{#if this.repository}}{{~this.repository}}{{else}}{{~@root.repository}}{{/if}}";

const releaseNotesGeneratorConfig = {
  preset: "conventionalcommits",
  parserOpts: {
    noteKeywords: ["BREAKING CHANGE", "BREAKING-CHANGE"],
  },
  writerOpts: {
    commitsSort(lhs, rhs) {
      let result = (lhs.scope ?? "").localeCompare(rhs.scope ?? "");
      if (!lhs.scope || !rhs.scope) {
        result = -result;
      }

      if (result === 0) {
        result = (lhs.subject ?? "").localeCompare(rhs.subject ?? "");
        if (!lhs.subject || !rhs.subject) {
          result = -result;
        }
      }
      return result;
    },
    notesSort(lhs, rhs) {
      let result = (lhs.commit.scope ?? "").localeCompare(rhs.commit.scope ?? "");
      if (!lhs.commit.scope || !rhs.commit.scope) {
        result = -result;
      }

      if (result === 0) {
        result = (lhs.text || "").localeCompare(rhs.text || "");
        if (!lhs.text || !rhs.text) {
          result = -result;
        }
      }
      return result;
    },
    issueUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/issues/{{this.issue}}`,
    commitUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/commit/{{hash}}`,
    compareUrlFormat: `${hostFormat}/${ownerFormat}/${repositoryFormat}/compare/{{previousTag}}...{{currentTag}}`,
    userUrlFormat: `${hostFormat}/{{user}}`,
    issuePrefixes: ["#"],
  },
  presetConfig: {
    types: sectionMap(),
  },
};

async function rstChangelog(pluginConfig, context) {
  const rstTemplatesDir = resolve(repoDir, ".gitlab", "assets", "release", "rst-templates");

  const config = {
    ...pluginConfig,
    ...releaseNotesGeneratorConfig,
    writerOpts: {
      ...(typeof pluginConfig["writerOpts"] === "object" ? pluginConfig["writerOpts"] : {}),
      ...releaseNotesGeneratorConfig.writerOpts,
      mainTemplate: await readFile(resolve(rstTemplatesDir, "template.hbs"), "utf8"),
      headerPartial: (await readFile(resolve(rstTemplatesDir, "header.hbs"), "utf8")).replace(
        /{{compareUrlFormat}}/g,
        releaseNotesGeneratorConfig.writerOpts.compareUrlFormat
      ),
      commitPartial: (await readFile(resolve(rstTemplatesDir, "commit.hbs"), "utf8"))
        .replace(/{{commitUrlFormat}}/g, releaseNotesGeneratorConfig.writerOpts.commitUrlFormat)
        .replace(/{{issueUrlFormat}}/g, releaseNotesGeneratorConfig.writerOpts.issueUrlFormat),
      footerPartial: "",
    },
  };

  Handlebars.registerHelper({
    formatNote: function (text) {
      // If the note contains a list keep it, otherwise join the lines.
      const lines = text.split("\n");
      if (lines.every((line) => ["* ", "+ ", "- ", "• ", "‣ ", "⁃ ", "  "].some((prefix) => line.startsWith(prefix)))) {
        return "\n\n" + lines.map((line) => "  " + line).join("\n");
      }
      return lines.join(" ");
    },
    underline: function (char, options) {
      const text = options.fn(this);
      return text + "\n" + char.repeat(text.length);
    },
    reformatMarkdown: function (options) {
      let text = options.fn(this);

      // inline code
      text = text.replace(
        /(`[^`]*?`)(.?)/g,
        (_, inlineCode, suffix) => inlineCode + ":code:" + (/^\s?$/.test(suffix) ? "" : "\\") + suffix
      );
      // urls
      text = text.replace(/\[([^\]]*?)]\((.*?)\)/g, "`$1 <$2>`_");

      return text;
    },
  });

  try {
    return (await generateNotes(config, context)).trim();
  } finally {
    Handlebars.unregisterHelper("formatNote");
    Handlebars.unregisterHelper("underline");
  }
}

function helm(...args) {
  console.log(execFileSync("helm", args, { encoding: "utf8" }));
}

async function getPackagedChartPath() {
  const chart = yaml.load(await readFile(commitAssetPaths.chartYamlPath, "utf8"), {
    schema: yaml.FAILSAFE_SCHEMA,
  });

  return resolve(repoDir, `${chart.name}-${chart.version}.tgz`);
}

async function helmPackage() {
  helm("dependency", "build", chartPath);
  helm("package", "--destination", repoDir, chartPath);

  const packagedChartPath = await getPackagedChartPath();
  await access(packagedChartPath, constants.F_OK | constants.R_OK);
}

function helmPush(packagedChartPath, channel) {
  const helmRepoName = "bbbatscale";
  helm(
    "repo",
    "add",
    "--username",
    "gitlab-ci-token",
    "--password",
    process.env["CI_JOB_TOKEN"] ?? "",
    helmRepoName,
    `${process.env["CI_API_V4_URL"] ?? ""}/projects/${process.env["CI_PROJECT_ID"] ?? ""}/packages/helm/${channel}`
  );
  helm("cm-push", packagedChartPath, helmRepoName);
}

module.exports = {
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        preset: "conventionalcommits",
        releaseRules: releaseRules(),
        parserOpts: {
          noteKeywords: BREAKING_CHANGE_TOKEN,
        },
      },
    ],
    ["@semantic-release/release-notes-generator", releaseNotesGeneratorConfig],
    [
      "@semantic-release/gitlab",
      {
        gitlabUrl: "https://gitlab.com/",
      },
    ],
    {
      async prepare(_, { nextRelease }) {
        const chart = yaml.load(await readFile(commitAssetPaths.chartYamlPath, "utf8"), {
          schema: yaml.FAILSAFE_SCHEMA,
        });

        chart.version = nextRelease.version;
        await writeFile(commitAssetPaths.chartYamlPath, yaml.dump(chart));

        await helmPackage();
      },
      async publish(_, { branch: { name, prerelease = false } }) {
        const channel = typeof prerelease === "boolean" ? (prerelease ? name : "stable") : prerelease;
        await helmPush(await getPackagedChartPath(), channel);
      },
    },
    {
      async prepare(pluginConfig, context) {
        const changelogHandle = await open(commitAssetPaths.changelogPath, "a");

        try {
          if ((await changelogHandle.stat()).size === 0) {
            await changelogHandle.write(
              "===========\n Changelog\n===========\n\n.. contents:: Versions\n  :depth: 1\n",
              undefined,
              "utf8"
            );
          }

          await changelogHandle.write("\n" + (await rstChangelog(pluginConfig, context)) + "\n", undefined, "utf8");
        } finally {
          await changelogHandle.close();
        }
      },
    },
    [
      "@semantic-release/git",
      {
        assets: Object.values(commitAssetPaths).map((commitAssetPath) => relative(repoDir, commitAssetPath)),
        message:
          "release(<%= nextRelease.version %>): auto generated release version <%= nextRelease.version %> at <%= new Date().toISOString() %>\n\n<%= nextRelease.notes %>",
      },
    ],
  ],
  tagFormat: "${version}",
  branches: ["main", { name: "beta", prerelease: true }, { name: "alpha", prerelease: true }],
};
