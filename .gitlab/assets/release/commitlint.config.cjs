"use strict";

const { BREAKING_CHANGE_TOKEN, typeScopeMap } = require("./commit-release-rules.cjs");

module.exports = {
  extends: ["@commitlint/config-conventional"],
  ignores: [
    (commit) =>
      /^release\((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\): /.test(
        commit
      ),
  ],
  rules: {
    "type-enum": [0],
    "type-scope-enum": [2, "always"],
    "subject-case": [2, "never", ["sentence-case", "start-case", "pascal-case", "upper-case"]],
    "subject-full-stop": [0],
    "subject-trailing-punctuation": [2, "never"],
    "scope-blank": [2, "never"],
    "body-leading-blank": [2, "always"],
    "footer-leading-blank": [2, "always"],
    "trailing-whitespace": [2, "never"],
    "body-contains-breaking-change-token": [2, "never"],
  },
  helpUrl: `${process.env["CI_PROJECT_URL"] ?? ""}/-/blob/main/CONTRIBUTING.rst`,
  plugins: [
    {
      rules: {
        "type-scope-enum": ({ type, scope }, when = "always", value = typeScopeMap()) => {
          if (type === null) {
            return [false, "expected a type but got null"];
          }

          const negated = when === "never";
          const validScopes = value.get(type);
          let valid;
          let message;

          if (Array.isArray(validScopes)) {
            const validScopesFiltered = validScopes.filter((validScope) => validScope !== null).sort();

            if (validScopesFiltered.length === 0) {
              valid = scope === null;
              message = `scope ${negated ? "must" : "may not"} be supplied`;
            } else {
              const acceptsOptionalScope = validScopes.length !== validScopesFiltered.length;
              valid = validScopesFiltered.includes(scope) || (scope === null && acceptsOptionalScope);

              if (acceptsOptionalScope) {
                message = `scope ${
                  negated ? "must be supplied and not" : "may not be supplied or"
                } one of: ${validScopesFiltered.join(", ")}`;
              } else {
                message = `scope ${negated ? "may not" : "must"} be one of: ${validScopesFiltered.join(", ")}`;
              }
            }
          } else {
            valid = false;
            message = `type should be one of: ${Array.from(value.keys()).sort().join(", ")}`;
          }

          return [negated ? !valid : valid, message];
        },

        "subject-trailing-punctuation": ({ subject }, when = "never", value = ["?", ":", "!", ".", ",", ";"]) => {
          const negated = when === "never";

          if (!subject) {
            return [negated, `subject must end with one of: ${value.join(" ")}`];
          }

          const lastCharacter = subject[subject.length - 1];
          const hasPunctuation = lastCharacter !== undefined && value.includes(lastCharacter);

          return [
            negated ? !hasPunctuation : hasPunctuation,
            `subject ${negated ? "may not" : "must"} end with one of: ${value.join(" ")}`,
          ];
        },

        "scope-blank": ({ header, type, scope }, when = "never") => {
          const negated = when === "never";

          if (scope !== null) {
            return [negated, `scope must be blank (only left and right parenthesis)`];
          }

          const hasBlankScope = header.startsWith((type ?? "") + "()");

          return [
            negated ? !hasBlankScope : hasBlankScope,
            `scope ${negated ? "may not" : "must"} be blank (only left and right parenthesis)`,
          ];
        },

        "trailing-whitespace": ({ raw }, when = "never", value = [" ", "\t"]) => {
          const negated = when === "never";

          const validate = (line) => {
            const lastCharacter = line[line.length - 1];
            return lastCharacter !== undefined && value.includes(lastCharacter);
          };
          const invalidLines = raw
            .split("\n")
            .map((line, index) => [line, index + 1])
            .filter(([line]) => (negated ? validate(line) : !validate(line)));

          const toUnicode = (character) => "U+" + ("000" + character.charCodeAt(0).toString(16)).slice(-4);

          return [
            invalidLines.length === 0,
            `line${invalidLines.length === 1 ? "" : "s"} ${invalidLines.map(([, index]) => index).join(", ")} ${
              negated ? "may not" : "must"
            } end with one of: ${value.map(toUnicode).join(", ")}`,
          ];
        },

        "body-contains-breaking-change-token": ({ body }, when = "never", value = BREAKING_CHANGE_TOKEN) => {
          const negated = when === "never";

          const valid = body !== null && value.some((element) => body.includes(element));

          return [negated ? !valid : valid, `body ${negated ? "may not" : "must"} contain one of: ${value.join(", ")}`];
        },
      },
    },
  ],
};
