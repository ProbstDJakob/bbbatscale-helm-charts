"use strict";

const NO_RELEASE = false;
const PATCH = "patch";
const MINOR = "minor";
const MAJOR = "major";

const commonScopes = ["templates", "values"];
const commitReleaseRules = new Map([
  [
    "docs",
    {
      section: null,
      release: NO_RELEASE,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "perf",
    {
      section: "Other",
      release: PATCH,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "test",
    {
      section: null,
      release: NO_RELEASE,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "refactor",
    {
      section: "Other",
      release: PATCH,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "chore",
    {
      section: "Other",
      release: PATCH,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "feat",
    {
      section: "Features",
      release: MINOR,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
  [
    "fix",
    {
      section: "Bug Fixes",
      release: PATCH,
      scopes: new Set([null, ...commonScopes]),
    },
  ],
]);

module.exports.typeScopeMap = () => {
  const typeScopeMap = new Map([["revert", new Set(commitReleaseRules.keys())]]);

  commitReleaseRules.forEach(({ scopes }, type) => {
    typeScopeMap.set(type, new Set(scopes));
  });

  return typeScopeMap;
};

module.exports.releaseRules = () => {
  const releaseRules = [{ breaking: true, release: MAJOR }];

  commitReleaseRules.forEach(({ release }, type) => {
    releaseRules.push(
      {
        type,
        release,
      },
      {
        type: "revert",
        scope: type,
        release,
      }
    );
  });

  return releaseRules;
};

module.exports.sectionMap = () => {
  const sectionMap = [{ type: "revert", section: "Reverts" }];

  commitReleaseRules.forEach(({ section }, type) => {
    sectionMap.push(section ? { type, section } : { type, hidden: true });
  });

  return sectionMap;
};

module.exports.BREAKING_CHANGE_TOKEN = ["BREAKING CHANGE", "BREAKING-CHANGE"];
